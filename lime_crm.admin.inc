<?php

/**
 * Settings form for Assistera webservice configurations
 */
function lime_crm_config_form($form, &$form_state) {
  $form = array();

  $form['lime_crm_endpoint_wsdl'] = array(
    '#title' => t('Url to Lime CRM webservice WSDL description'),
    '#type' => 'textfield',
    '#default_value' => variable_get('lime_crm_endpoint_wsdl', ''),
    '#description' => t('Provide url to accessible Lime CRM webservice description, e.g. "http://127.0.0.1/test?wsdl". There should be valid XML description of SOAP webservice.'),
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['test_connection'] = array(
    '#type' => 'button',
    '#value' => t('Test endpoint'),
  );

  $form['#validate'] = array('lime_crm_config_form_connection_validate');

//  $lime = LimeCRM::getInstance();

 // Get file
//  $lime_result = $lime->findFile(array(array('operator' => '=','field' => 'iddocument','value' => '5009601')));
//  $lime_result = $lime_result['@attributes'];
//
//  // @todo lime_files dir
//  $binary = base64_decode($lime_result['document__data']);
//  $file = file_save_data($binary, 'public://lime_files/' . $lime_result['comment'] . '.' . $lime_result['document__fileextension']);
  //push user
  //global $user;
//  $user = user_load(80);
//  lime_crm_crud_push($user, 'user', 'person');

  //pull user
//  $entity = new stdClass();
//  $entity->lc_entity_id = '1330401';
//
//  $lime_result = lime_crm_crud_pull($entity, 'user', 'person');

  // pull company
//  $entity = new stdClass();
//  $entity->field_company_id = '416501';
//
//  $lime_result = lime_crm_crud_pull($entity, 'company', 'company');

  return system_settings_form($form);
}

/**
 * Validate test connection to SOAP wsdl
 */
function lime_crm_config_form_connection_validate($form, &$form_state) {
  if (!empty($form_state['triggering_element']['#value']) && $form_state['triggering_element']['#value'] == t('Test endpoint')) {
    $limeCRM = LimeCRM::getInstance($form_state['values']['lime_crm_endpoint_wsdl']);

    $entity = new stdClass();
    $entity->lc_entity_id = '1330401';

    $lime_result = lime_crm_crud_pull($entity, 'user', 'person');

    $result = $limeCRM->testConnection();

    if (!$result) {
      form_set_error('lime_crm_endpoint_wsdl', t('Connection to @ip failed.', array('@ip' => $form_state['values']['lime_crm_endpoint_wsdl'])));
    }
    else {
      variable_set('lime_crm_endpoint_wsdl', $form_state['values']['lime_crm_endpoint_wsdl']);
    }
  }
}