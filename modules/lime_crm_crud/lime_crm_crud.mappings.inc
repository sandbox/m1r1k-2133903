<?php

/**
 * Relations Drupal person with Lime CRM.
 */
function lime_person_mapping() {
  $return['main'] = array(
    'lc_entity_id' => 'idperson',
    'mail' => 'email',
  );

  $return['profile'] = array(
    'field_profile_full_name' => 'name',
    'field_profile_phone' => 'phone',
    'field_orgnr_persnr' => 'company',
    'field_profile_position' => 'position',
    'field_profile_other_location' => 'region',
  );

  $return['files'] = array(
    'field_profile_other_cv' => '234001',
  );

  $return['file_fields_from_crm'] = array(
    'iddocument',
    'document',
    'document__data',
    'comment',
    'person',
    'documenttype',
  );

  $return['uid'] = array(
    'uid' => 'd_entity_id'
  );

  return $return;
}

/**
 * Relations Drupal company with Lime CRM.
 */
function lime_company_mapping() {
  $return['main'] = array(
    'title' => 'name',
    'field_company_id' => 'idcompany',
    'field_sales_person' => 'signatory',
  );

  $return['files'] = array(
    'field_vat_files' => '302401',
  );

  $return['uid'] = array(
    'uid' => 'd_entity_id'
  );

  return $return;
}
/**
 * PLEASE FILL ME.
 */
function lime_field_type_mapping($field_name) {
  $mapping =  array(
    'idperson' => 'numeric',
    'idcompany' => 'numeric',
    'iddocument' => 'numeric',
    'person' => 'numeric',
    'documenttype' => 'numeric',
    'company' => 'numeric'
  );

  return isset($mapping[$field_name]) ? $mapping[$field_name] : 'string';
}

/**
 * Prepare
 * @param $person
 */
function lime_prepare_drupal_person_data(&$person){
  $person->profile = profile2_load_by_user($person, 'main');

  // Shortcut
  $region = $person->profile->field_profile_other_location[LANGUAGE_NONE];

  // @todo i hope this can implemented other better way
  $region = array_map('lime_array_get_childrens', $region);
  $implode = ';' . implode(';', $region) . ';';
  $person->profile->field_profile_other_location[LANGUAGE_NONE] = array(array('value' => $implode));

}

/**
 * Get $lime_data with lime_person_mapping() from $person profile.
 */
function lime_get_drupal_person_data(&$person) {
  lime_prepare_drupal_person_data($person);
  $lime_data = array();

  $fields = lime_person_mapping();
  $fields = array_merge($fields['main'], $fields['profile'], $fields['uid']);

  // dfield => drupal field;
  // lcfield => lime crm field
  foreach ($fields as $dfield => $lcfield) {
    if (property_exists($person, $dfield)) {
      $lime_data[$lcfield] = $person->{$dfield};
    }
    elseif (property_exists($person->profile, $dfield) && !empty($person->profile->{$dfield}[LANGUAGE_NONE][0]['value'])) {
      $lime_data[$lcfield] = $person->profile->{$dfield}[LANGUAGE_NONE][0]['value'];
    }
    else {
      $lime_data[$lcfield] = '';
    }
  }

  return $lime_data;
}


/**
 * Get $lime_data with lime_person_mapping() from $company.
 */
function lime_get_drupal_company_data(&$company) {
  //lime_prepare_drupal_person_data($person);
  $lime_data = array();

  $fields = lime_company_mapping();
  $fields = array_merge($fields['main'], $fields['uid']);

  // dfield => drupal field;
  // lcfield => lime crm field
  foreach ($fields as $dfield => $lcfield) {
    if (property_exists($company, $dfield)) {
      $lime_data[$lcfield] = $company->{$dfield};
    }
//    elseif (property_exists($company->profile, $dfield) && !empty($company->profile->{$dfield}[LANGUAGE_NONE][0]['value'])) {
//      $lime_data[$lcfield] = $company->profile->{$dfield}[LANGUAGE_NONE][0]['value'];
//    }
    else {
      $lime_data[$lcfield] = '';
    }
  }

  return $lime_data;
}

function lime_array_get_childrens($elem){
  return $elem['value'];
}
/**
 * PLEASE KILL ME
 */
function lime_get_lime_person_data($person) {

}