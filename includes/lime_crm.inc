<?php
/**
 * @file
 * Class prepare query's and do soap call.
 */

/**
 * Class LimeCRM provided BLA BLA BLA.
 */
class LimeCRM {

  protected $limeCRMQuery;

  protected $soapClient;

  protected $endpointUrl;

  /**
   * Default constructor.
   *
   * @param string $endpoint
   *   Endpoint??? maybe url to soap service
   */
  protected function __construct($endpoint = '') {
    $this->endpointUrl = $endpoint ? $endpoint : variable_get('lime_crm_endpoint_wsdl');

    $this->limeCRMQuery = new LimeCRMQuery();

    $options = array(
      'trace' => 1,
      'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
      'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
      'cache_wsdl' => WSDL_CACHE_NONE,
    );

    $this->soapClient = new SoapClient($this->endpointUrl, $options);
  }

  /**
   * Get instance of BLA BLA.
   */
  static public function getInstance($endpoint = '') {
    $instance = &drupal_static(__FUNCTION__);

    if (empty($instance)) {
      $instance = new self($endpoint);
    }

    return $instance;
  }

  /**
   * Test connection to Lime CRM.
   */
  public function testConnection() {
    $query = $this->limeCRMQuery
      ->selectPersons(array('top' => 2))
      ->addFields(array('idperson', 'name', 'email'))
      ->build();

    $result = $this->soapCall($query);

    return !empty($result);
  }

  /**
   * Find one person with $condition and return idperson if exist.
   */
  public function findPerson($conditions) {
    $query = $this->limeCRMQuery
      ->selectPersons(array('top' => 1))
      ->addFields(array('idperson'))
      ->addConditions($conditions)
      ->build();

    $result = $this->soapCall($query);

    return ($result && $result->count() ? (string) $result->person[0]['idperson'] : FALSE);
  }

  /**
   * Pull all fields specified in lime_person_mapping().
   */
  public function pullPerson($conditions) {
    $fields = lime_person_mapping();
    $fields = array_values(array_merge($fields['main'], $fields['profile']));

    $query = $this->limeCRMQuery
      ->selectPersons(array('top' => 1))
      ->addFields($fields)
      ->addConditions($conditions)
      ->build();

    $result = $this->soapCall($query);

    return ($result && $result->count() ? (array) $result->person[0] : FALSE);
  }

  /**
   * Pull all fields specified in lime_person_mapping().
   */
  public function pullCompany($conditions) {
    $fields = lime_company_mapping();
    $fields = array_values(array_merge($fields['main']));

    $query = $this->limeCRMQuery
      ->selectCompanies(array('top' => 1))
      ->addFields($fields)
      ->addConditions($conditions)
      ->build();

    $result = $this->soapCall($query);

    return ($result && $result->count() ? (array) $result->company[0] : FALSE);
  }

  /**
   * Find one company with $condition and return idcompany if exist.
   */
  public function findCompany($conditions) {
    $query = $this->limeCRMQuery
      ->selectCompanies(array('top' => 1))
      ->addFields(array('idcompany'))
      ->addConditions($conditions)
      ->build();

    $result = $this->soapCall($query);

    return ($result && $result->count() ? (string) $result->company[0]['idcompany'] : FALSE);
  }

  /**
   * Pull all fields specified in lime_person_mapping().
   */
  public function findFile($conditions) {
    module_load_include('inc', 'lime_crm_crud', 'lime_crm_crud.mappings');

    $mapping = lime_person_mapping();
    $query = $this->limeCRMQuery
      ->selectDocuments()
      ->addFields($mapping['file_fields_from_crm'])
      ->addConditions($conditions)
      ->build();

    $result = $this->soapCall($query);

    return ($result && $result->count() ? (array) $result : FALSE);
  }

  /**
   * Push one or more person to Lime crm.
   */
  public function pushPerson($records) {
    $query = $this->limeCRMQuery
      ->update($records)
      ->addRecord('person', $records)
      ->build();

    return $this->soapCall($query, 'update');
  }

  /**
   * Push one or more company to Lime crm.
   */
  public function pushCompany($records) {
    $query = $this->limeCRMQuery
      ->update($records)
      ->addRecord('company', $records)
      ->build();

    return $this->soapCall($query, 'update');
  }

  /**
   * Push one file to Lime crm.
   */
  public function pushFile($records) {
    $query = $this->limeCRMQuery
      ->update($records, 'records')
      ->startRecord('document', $records['parent_record'])
        ->addRecord('document', array($records['child_record']))
      ->endRecord()
      ->build();

    return $this->soapCall($query, 'update');
  }

  /**
   * THIS IS SOAP CALL AND THEY DO SOAP CALL.
   */
  public function soapCall($query, $type = 'get') {
    $data = FALSE;

    try {
      switch ($type) {
        case 'get':
          $query = array('query' => new SoapVar($query, XSD_STRING));
          $data = $this->soapClient->GetXMLQueryData($query);
          $this->dpmQuery($query, 'get');
          break;

        case 'update':
          $query = array('data' => new SoapVar($query, XSD_STRING));

          $data = $this->soapClient->UpdateData($query);

          break;
      }
    }
    catch (Exception $e) {
      var_dump('Exception!: ' . $e->getMessage());
    }
    $this->dpmQuery($query, $type);
    return $this->processXMLData($data, $type);
  }

  /**
   * OH I AM REALLY GOOD IN PROCESSING XML DATA.
   */
  protected function processXMLData($data, $type) {
    $result = FALSE;

    switch ($type) {
      case 'get':
        $result = simplexml_load_string(str_replace('<?xml version="1.0" encoding="UTF-16" ?>', '', $data->GetXmlQueryDataResult));
        break;

      case 'update':
        $result = simplexml_load_string(str_replace('<?xml version="1.0" encoding="UTF-16" ?>', '', $data->UpdateDataResult));
        if ($result && $result->count()) {
          $result = $this->processDataUpdateResponse($result);
        }
        break;
    }

    return $result;
  }

  /**
   * NICE ONE OF DOC.
   */
  protected function processDataUpdateResponse(SimpleXMLElement $response) {
    return (!empty($response->record[0]['idnew']) ? (string) $response->record[0]['idnew'] : FALSE);
  }

  /**
   * DPM IT ALL.
   */
  protected function dpmQuery($query, $name = '') {
    dpm(array(
      'query' => $query,
      'functions' => $this->soapClient->__getFunctions(),
      'last_request_headers' => $this->soapClient->__getLastRequestHeaders(),
      'last_request' => $this->soapClient->__getLastRequest(),
      'last_response' => $this->soapClient->__getLastResponse(),
      'last_response_headers' => $this->soapClient->__getLastResponseHeaders(),
    ), $name);
  }
}
