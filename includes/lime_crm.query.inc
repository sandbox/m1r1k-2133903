<?php
/**
 * @file
 * Class contains query and function for operate with it.
 */

/**
 * Class LimeCRMQuery I AM A COOL CLASS
 */
class LimeCRMQuery {
  protected $xmlWriter;
  protected $query;

  /**
   * Base comment.
   */
  public function __construct() {
    $this->xmlWriter = new xmlWriter();

    return $this;
  }

  /**
   * SELECT IT ALL.
   */
  public function select($type, $options = array()) {
    $this->initDefaultXMLWriter($options);

    $this->xmlWriter->startElement("query");
    $this->xmlWriter->writeAttribute("distinct", "1");

    foreach ($options as $attribute => $value) {
      $this->xmlWriter->writeAttribute($attribute, $value);
    }

    $this->xmlWriter->startElement("tables");
    $this->xmlWriter->writeElement("table", $type);
    $this->xmlWriter->endElement();

    return $this;
  }

  /**
   * Update it.
   */
  public function update($options = array(), $type = 'data') {
    $this->initDefaultXMLWriter($options);

    $this->xmlWriter->startElement($type);

    return $this;
  }

  /**
   * Start record.
   */
  public function startRecord($type, $record) {
    $this->xmlWriter->startElement($type);

    foreach ($record as $attribute => $value) {
      $this->xmlWriter->writeAttribute($attribute, $value);
    }

    return $this;
  }
  /**
   * ADD some record.
   */
  public function addRecord($type, $records) {
    foreach ($records as $key => $data) {
      $this->xmlWriter->startElement($type);

      foreach ($data as $attribute => $value) {
        if ($attribute == 'xml_element_text') {
          $this->xmlWriter->text($value);
          continue;
        }
        $this->xmlWriter->writeAttribute($attribute, $value);
      }

      $this->xmlWriter->endElement();
    }

    return $this;
  }

  /**
   * End record.
   */
  public function endRecord() {
    $this->xmlWriter->endElement();

    return $this;
  }

  /**
   * PLEASE FILL ME.
   */
  public function addFields($fields) {
    $this->xmlWriter->startElement("fields");

    foreach ($fields as $field_name) {
      $this->xmlWriter->writeElement("field", $field_name);
    }

    $this->xmlWriter->endElement();

    return $this;
  }

  /**
   * ADD SOME CONDITIONS.
   */
  public function addConditions($conditions) {
    if (empty($conditions)) {
      return $this;
    }

    $this->xmlWriter->startElement("conditions");

    foreach ($conditions as $condition) {
      $this->addCondition($condition);
    }

    $this->xmlWriter->endElement();

    return $this;
  }

  /**
   * Add some condition.
   */
  protected function addCondition($condition) {
    module_load_include('inc', 'lime_crm_crud', 'lime_crm_crud.mappings');
    switch ($condition['operator']) {
      case '=':
        $this->xmlWriter->startElement("condition");
          $this->xmlWriter->writeAttribute("operator", $condition['operator']);
          $this->xmlWriter->startElement("exp");
          $this->xmlWriter->writeAttribute("type", "field");
            $this->xmlWriter->text($condition['field']);
          $this->xmlWriter->endElement();
          $this->xmlWriter->startElement("exp");
        // TODO remove external dependency
          $field_type = lime_field_type_mapping($condition['field']);
          $this->xmlWriter->writeAttribute("type", $field_type);
            $this->xmlWriter->text($condition['value']);
          $this->xmlWriter->endElement();
        $this->xmlWriter->endElement();
        break;

      case 'in':
        break;
    }
  }

  /**
   * Init default XML writer.
   */
  public function initDefaultXMLWriter($options = array()) {
    $this->xmlWriter->openMemory();
    $this->xmlWriter->startDocument("1.0");
  }

  /**
   * Build result string.
   * @return string
   */
  public function build() {
    $this->xmlWriter->endDocument();

    $this->query = $this->xmlWriter->outputMemory();

    return $this->query;
  }

  /**
   *
   */
  public function selectPersons($options = array()) {
    return $this->select('person', $options);
  }

  public function selectCompanies($options = array()) {
    // Visibility.
    return $this->select('company', $options);
  }

  public function selectDocuments($options = array()) {
    // Visibility.
    return $this->select('document', $options);
  }
}